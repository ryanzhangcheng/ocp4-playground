rbac-lab.adoc 来自有：
https://github.com/RedHatDemos/SecurityDemos/blob/master/2021Labs/OpenShiftSecurity/documentation/lab3.adoc

## 第一部分实验
这个实验不需要特殊安装，任何OCP上的普通pod 就可以运行， 就是通过pod 加载证书和token进行 kubernetes api访问 的过程.

每一个OCP/K8中的pod 都自动会加载 如下证书
```
sh-4.2$ pwd
/var/run/secrets/kubernetes.io/serviceaccount
sh-4.2$ ls -al
total 0
drwxrwsrwt. 3 root 1000640000 160 Mar 24 07:12 .
drwxr-xr-x. 3 root root        60 Mar 24 07:12 ..
drwxr-sr-x. 2 root 1000640000 120 Mar 24 07:12 ..2022_03_24_07_12_32.495413499
lrwxrwxrwx. 1 root root        31 Mar 24 07:12 ..data -> ..2022_03_24_07_12_32.495413499
lrwxrwxrwx. 1 root root        13 Mar 24 07:12 ca.crt -> ..data/ca.crt
lrwxrwxrwx. 1 root root        16 Mar 24 07:12 namespace -> ..data/namespace
lrwxrwxrwx. 1 root root        21 Mar 24 07:12 service-ca.crt -> ..data/service-ca.crt
lrwxrwxrwx. 1 root root        12 Mar 24 07:12 token -> ..data/token
```

## 第二部分 是通过RBAC 给rbac-lab的default serviceaccount绑定 不同权限
这里面需要用到src下面的一个go程序 访问kubernetes 不同的资源.

安装方法
```
oc new-project rbac-lab
oc apply -f .
```

这里面主要学习如何给各种资源定义角色，和角色绑定

还可以使用oc auth can-i list user --as 这样的命令 试探 账号权限

