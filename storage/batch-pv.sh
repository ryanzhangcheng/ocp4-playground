#/bin/bash

for i in {3..20};
do
cat <<< """
apiVersion: v1
kind: PersistentVolume
metadata:
  name: user$i-pv
  labels:
    type: local
spec:
  storageClassName: manual
  capacity:
    storage: 10Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/data/user$i"
"""|kubectl apply -f -

sleep 2
done
