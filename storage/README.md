配置NAS 动态存储（Stroage Class）
文档: https://github.com/kubernetes-sigs/nfs-subdir-external-provisioner 
oc new-project nfs-provisioner

oc adm policy add-scc-to-user hostmount-anyuid system:serviceaccount:$NAMESPACE:nfs-subdir-external-provisioner

helm install nfs-subdir-external-provisioner nfs-subdir-external-provisioner/nfs-subdir-external-provisioner \
                                                      --set nfs.server=nfs.lmc \
                                                      --set nfs.path=/exports/ocp1.lmc.io \
                                                      --set nodeSelector.node-role\\.kubernetes\\.io/infra=
NAME: nfs-subdir-external-provisioner
LAST DEPLOYED: Tue Aug  2 17:25:56 2022
NAMESPACE: nfs-provisioner
STATUS: deployed
REVISION: 1
TEST SUITE: None



