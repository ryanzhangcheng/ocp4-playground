# 调试大法
增加root权限
```
oc adm policy add-scc-to-user previleged -z default
clusterrole.rbac.authorization.k8s.io/system:openshift:scc:previleged added: "default"

# 给mutitool-deployment打开anyuid 权限
oc adm policy add-scc-to-user anyuid -z default
```

mutitool 如果无法从docker.io下载 可以从quay.io/rzhang/network-multitool下载，也可以通过登录docker.io下载;

# oc 调试命令
```
oc debug $POD_NAME --as-root
```
